#ifndef PRESSURE_H
#define PRESSURE_H

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/logging/log.h>
#include <zephyr/drivers/sensor.h>  // prf.conf -> CONFIG_SENSOR=y

int read_pressure_sensor(const struct device *pressure_in, int oversample, int atm_offset_kPa);

#endif
