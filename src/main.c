/* BME 590: Final Project*/

/* -----------------------------------------------------------------------------------------------------------   */

/* SECTION 1: INCLUDE LIBRARIES and LOG_MODULE_REGISTER */

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/pwm.h>
#include <zephyr/drivers/sensor.h>
#include <nrfx_power.h>
#include "pressure.h"

LOG_MODULE_REGISTER(main, LOG_LEVEL_INF);

/* -----------------------------------------------------------------------------------------------------------   */

/* SECTION 2: MACROS */

/* Heartbeat, ADC, and Pressure Macros */

#define HEARTBEAT_TIME_MS 500
#define LED3_BLINK_TIME_MS 500
#define BRIGHT1_SAMPLING_MS 1
#define BRIGHT2_SAMPLING_US 200
#define SENSOR_CHECK_DURATION_MS 600
#define VBAT_CHECK_S 10

#define ADC_DT_SPEC_GET_BY_ALIAS(adc_alias)                   \
    {                                                         \
        .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(adc_alias))), \
        .channel_id = DT_REG_ADDR(DT_ALIAS(adc_alias)),       \
        ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(adc_alias))     \
    }

#define OVERSAMPLE 10

/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/

#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

/* -----------------------------------------------------------------------------------------------------------  */

/* SECTION 3: Global Variables, ADC and PWM Definitions and Initializations, State Table, Timers, and Callbacks*/

/* Global variable */
double max_bright_led1;
double max_bright_led2; 
double bright_led1;
double bright_led2; 
int counter;
int32_t atm_pressure_kPa;
int32_t val_mv_array[1000];
int32_t val_mv_array_2[5000];
int32_t max_sum;
int32_t min_sum;
int32_t period_max_val;
int32_t period_min_val; 
int32_t avg_max_val;
int32_t avg_min_val; 
int32_t avg_vpp; 
int32_t avg_vpp_2;
int32_t avg_max2_val;
int32_t avg_min2_val;
int period_counter_1;
int period_counter_2;
int loop_count_1; 
int loop_count_2;
bool val_arr_full;
bool val_arr2_full;
bool sensor_check_complete=false;
bool sensor_connected=false;
int recorder=0;
int32_t vbat_to_send=0;
int button1_array[3];

int32_t sanity_check;
int32_t sanity_check_2=0;

int32_t period_min_arr;
int32_t period_max_arr;
int32_t period_min_arr2;
int32_t period_max_arr2;
int32_t max_sum_2;
int32_t min_sum_2;

/* define PWM structs using DT aliases*/
static const struct pwm_dt_spec led1_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
static const struct pwm_dt_spec led2_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

/* Intialize the ADC struct to store all the DT parameters */
static const struct adc_dt_spec adc_vadc = ADC_DT_SPEC_GET_BY_ALIAS(vadc);
static const struct adc_dt_spec adc_vadc_1 = ADC_DT_SPEC_GET_BY_ALIAS(vadc1);
static const struct adc_dt_spec adc_vbat = ADC_DT_SPEC_GET_BY_ALIAS(vbat);

/* Forward declaration of state table */
static const struct smf_state device_states[];

/* List of device states */
enum device_states
{
    init_S,
    measure_S,
    vbus_S,
    error_S
};

/* User defined object */
struct s_object
{
    /* This must be first */
    struct smf_ctx ctx;

    /* Other state specific data add here */
} s_obj;

/*Declare timer start and stop functions*/
void heartbeat_start(struct k_timer *heartbeat_timer);
void led3_blink_start(struct k_timer *led3_blink_timer);
void led3_blink_stop(struct k_timer *led3_blink_timer);
void adjust_bright1_start(struct k_timer *adjust_bright1_timer);
void adjust_bright1_stop(struct k_timer *adjust_bright1_timer);
void adjust_bright2_start(struct k_timer *adjust_bright2_timer);
void adjust_bright2_stop(struct k_timer *adjust_bright2_timer);
//void sensor_check_start(struct k_timer *sensor_check);
void bright2_timer_work_handler(struct k_work *bright2_timer_work);
void bright1_timer_work_handler(struct k_work *bright1_timer_work);
void vbat_timer_start(struct k_timer *vbat_timer);
void vbat_timer_stop(struct k_timer *vbat_timer);
void vbat_timer_work_handler(struct k_work *vbat_timer_work);

/*Define Timers*/
K_TIMER_DEFINE(heartbeat_timer, heartbeat_start, NULL);
K_TIMER_DEFINE(led3_blink_timer, led3_blink_start, led3_blink_stop);
K_TIMER_DEFINE(adjust_bright1_timer, adjust_bright1_start, adjust_bright1_stop);
K_TIMER_DEFINE(adjust_bright2_timer, adjust_bright2_start, adjust_bright2_stop);
K_TIMER_DEFINE(sensor_check, NULL, NULL);
K_TIMER_DEFINE(vbat_timer, vbat_timer_start, vbat_timer_stop);

K_WORK_DEFINE(bright1_timer_work, bright1_timer_work_handler);   /*I think that heartbeat timer does not need a work handler because it should always be running?*/
K_WORK_DEFINE(bright2_timer_work, bright2_timer_work_handler);
K_WORK_DEFINE(vbat_timer_work, vbat_timer_work_handler);

/*Defining firmware variable names for LEDs */
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(DT_ALIAS(heartbeat), gpios);
static const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(DT_ALIAS(led1), gpios);
static const struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET(DT_ALIAS(led2), gpios);
static const struct gpio_dt_spec led3 = GPIO_DT_SPEC_GET(DT_ALIAS(led3), gpios);

/*Defining firmware variable names for buttons */
static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(DT_ALIAS(button1), gpios);
static const struct gpio_dt_spec button2 = GPIO_DT_SPEC_GET(DT_ALIAS(button2), gpios);

/* Pressure sensor definition and function */
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 


/* --------------------------------------------------------------------------------- */

/* SECTION 4: BLE Enumeration, Functions, Declarations (Unedited from source) */


// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 
static struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

/* Function Declarations */
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                BT_GATT_PERM_WRITE,
                NULL, on_write, NULL),
);

/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &button1_array, sizeof(button1_array));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

	// hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}

	return ret;

}

/* Function Declarations */
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

static struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}

/* --------------------------------------------------------------------------------- */



void heartbeat_start(struct k_timer *heartbeat_timer)
{

    gpio_pin_toggle_dt(&heartbeat_led);
}

void led3_blink_start(struct k_timer *led3_blink_timer)
{

    gpio_pin_toggle_dt(&led3);

}

void led3_blink_stop(struct k_timer *led3_blink_timer)
{
    //turn off LED3
    gpio_pin_set_dt(&led3, 0); 

}

void vbat_timer_start(struct k_timer *vbat_timer)
{
    
    k_work_submit(&vbat_timer_work);

}

void vbat_timer_work_handler(struct k_work *vbat_timer_work)
{

    int ret;
    int16_t buf_2;
    struct adc_sequence sequence_2 = {
        .buffer = &buf_2,
        .buffer_size = sizeof(buf_2), // bytes
    };
    //LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
    (void)adc_sequence_init_dt(&adc_vbat, &sequence_2);
    ret = adc_read(adc_vbat.dev, &sequence_2);
    if (ret < 0)
    {
        LOG_ERR("Could not read (%d)", ret);
    }
    else
    {
        
    }
    int32_t vbat_mv;
    vbat_mv = buf_2;
    ret = adc_raw_to_millivolts_dt(&adc_vbat, &vbat_mv); // remember that the vadc struct containts all the DT parameters
    if (ret < 0)
    {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    }
    else
    {
        
    }

    LOG_INF("Vbat voltage in mV is %d", vbat_mv);
    vbat_to_send = vbat_mv; 

}

void vbat_timer_stop(struct k_timer *vbat_timer){



}

void adjust_bright1_start(struct k_timer *adjust_bright1_timer){

    k_work_submit(&bright1_timer_work);

}

void bright1_timer_work_handler(struct k_work *bright1_timer_work)
{
    
    if (val_arr_full == false){
    // Read in differential voltage from AIN0 and AIN1 channels 
    int ret;
    int16_t buf;
    struct adc_sequence sequence = {
        .buffer = &buf,
        .buffer_size = sizeof(buf), // bytes
    };
    //LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
    (void)adc_sequence_init_dt(&adc_vadc, &sequence);
    ret = adc_read(adc_vadc.dev, &sequence);
    if (ret < 0)
    {
        LOG_ERR("Could not read (%d)", ret);
    }
    else
    {
        //LOG_DBG("Raw ADC Buffer: %d", buf_1);
    }
    int32_t val_mv;
    val_mv = buf;
    ret = adc_raw_to_millivolts_dt(&adc_vadc, &val_mv); // remember that the vadc struct containts all the DT parameters
    if (ret < 0)
    {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    }
    else
    {
        //LOG_INF("AIN0 ADC Value (mV): %d", val_mv);
    }

    //record read differential voltage in array
    val_mv_array[loop_count_1] = val_mv;
    if (loop_count_1>998){
        //loop count has reached 999, array has been completed with 1000 voltage values
        loop_count_1 = 0;
        //sanity_check = val_mv_1;
        val_arr_full = true;
        //LOG_INF("Val_arr_full is true");
    }
    else {
        loop_count_1+=1; 
    }

    }
    
    
}

void adjust_bright1_stop(struct k_timer *adjust_bright1_timer)
{
    
    int ret;
    bright_led1=0;
    ret = pwm_set_pulse_dt(&led1_pwm, (1-(float)bright_led1)*led1_pwm.period);

}

void adjust_bright2_start(struct k_timer *adjust_bright2_timer){

    k_work_submit(&bright2_timer_work);

}

void bright2_timer_work_handler(struct k_work *bright2_timer_work)
{
    if (val_arr2_full == false){
    // Read in differential voltage from AIN2 and AIN3 channels 
    int ret;
    int16_t buf_1;
    struct adc_sequence sequence_1 = {
        .buffer = &buf_1,
        .buffer_size = sizeof(buf_1), // bytes
    };
    //LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
    (void)adc_sequence_init_dt(&adc_vadc_1, &sequence_1);
    ret = adc_read(adc_vadc_1.dev, &sequence_1);
    if (ret < 0)
    {
        LOG_ERR("Could not read (%d)", ret);
    }
    else
    {
        //LOG_DBG("Raw ADC Buffer: %d", buf_1);
    }
    int32_t val_mv_1;
    val_mv_1 = buf_1;
    ret = adc_raw_to_millivolts_dt(&adc_vadc_1, &val_mv_1); // remember that the vadc struct containts all the DT parameters
    if (ret < 0)
    {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    }
    else
    {
        //LOG_INF("AIN0 ADC Value (mV): %d", val_mv);
    }

    //record read differential voltage in array
    val_mv_array_2[loop_count_2] = val_mv_1;
    if (loop_count_2>4998){
        //loop count has reached 4999, array has been completed with 5000 voltage values
        loop_count_2 = 0;
        sanity_check = val_mv_1;
        val_arr2_full = true;
        //LOG_INF("Val_arr2_full is true");
    }
    else {
        loop_count_2+=1; 
    }

    }
    
}

void adjust_bright2_stop(struct k_timer *adjust_bright2_timer)
{
    int ret;
    bright_led2=0;
    ret = pwm_set_pulse_dt(&led2_pwm, (1-(float)bright_led2)*led2_pwm.period);


}



/* Callback Functions for Buttons 1 and 2 */

void button1_pressed(const struct device *dev, struct gpio_callback *cb,
                         uint32_t pins)
{
    
    //Save current percent brightness and pressure sensor mean  - MUST BE WRITTEN!

    button1_array[0] = (float)bright_led1*100;
    button1_array[1] = (float)bright_led2*100;
    button1_array[2] = (float)atm_pressure_kPa;


}

static struct gpio_callback button1_cb_data;

void button2_pressed(const struct device *dev, struct gpio_callback *cb,
                         uint32_t pins)
{
    
// BLE Notification - MUST BE WRITTEN!



    // do stuff, write to "data" array

    // send a notification that "data" is ready to be read...
    // send a notification that "data" is ready to be read...
    int err;
    err = send_data_notification(current_conn, button1_array, 1);
    if (err) {
        LOG_ERR("Could not send BT notification (err: %d)", err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }


    /* Example of how to use the Battery Service
       `normalized_level` comes from an ADC reading of the battery voltage
       this function populates the BAS GATT with information
    */
    err = bt_bas_set_battery_level(vbat_to_send);
    if (err) {
        LOG_ERR("BAS set error (err = %d)", err);
    }

    // this function retrieves BAS GATT with information
    uint8_t battery_level;
    battery_level =  bt_bas_get_battery_level();


}

static struct gpio_callback button2_cb_data;

/* -----------------------------------------------------------------------------------------------------------   */




/* -----------------------------------------------------------------------------------------------------------   */

/* SECTION 5: STATE MACHINES */

/* State init_S */

static void init_S_entry(void *o)
{
    int ret;

    if (!gpio_is_ready_dt(&heartbeat_led))
    {
        return;
    }

    // configure LEDs

    ret = gpio_pin_configure_dt(&led1, GPIO_OUTPUT_INACTIVE);
    if (ret < 0)
    {
        return;
    }
    
    ret = gpio_pin_configure_dt(&led2, GPIO_OUTPUT_INACTIVE);
    if (ret < 0)
    {
        return;
    }

    ret = gpio_pin_configure_dt(&led3, GPIO_OUTPUT_INACTIVE);
    if (ret < 0)
    {
        return;
    }

    ret = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_INACTIVE);
    if (ret < 0)
    {
        return;
    }

    // configure buttons

    ret = gpio_pin_configure_dt(&button1, GPIO_INPUT);
    if (ret < 0)
    {
        return;
    }

    ret = gpio_pin_configure_dt(&button2, GPIO_INPUT);
    if (ret < 0)
    {
        return;
    }

    // Associating button press callbacks with corresponding GPIO pin

    ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&button1_cb_data, button1_pressed, BIT(button1.pin));
    gpio_add_callback(button1.port, &button1_cb_data);

    ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&button2_cb_data, button2_pressed, BIT(button2.pin));
    gpio_add_callback(button2.port, &button2_cb_data);

    /* Check that the ADC interface is ready */

    if (!device_is_ready(adc_vadc.dev))
    {
        LOG_ERR("ADC controller device(s) not ready");
        return -1;
    }

    /* Check that the PWM controller is ready */

    if (!device_is_ready(led1_pwm.dev))
    {
        LOG_ERR("PWM device %s is not ready.", led1_pwm.dev->name);
        return -1;
    }

    /* Configure the ADC channels */

    ret = adc_channel_setup_dt(&adc_vadc);
    if (ret < 0)
    {
        LOG_ERR("Could not setup ADC channel (%d)", ret);
        return ret;
    }

    ret = adc_channel_setup_dt(&adc_vadc_1);
    if (ret < 0)
    {
        LOG_ERR("Could not setup ADC channel (%d)", ret);
        return ret;
    }

    ret = adc_channel_setup_dt(&adc_vbat);
    if (ret < 0)
    {
        LOG_ERR("Could not setup ADC channel (%d)", ret);
        return ret;
    }

    /* Initialize Bluetooth */
    int err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (err) {
        LOG_ERR("BT init failed (err = %d)", err);
    }

    k_timer_start(&sensor_check, K_MSEC(SENSOR_CHECK_DURATION_MS), K_NO_WAIT);

}
static void init_S_run(void *o)
{
    // exit init and enter the next state
    int err;
    err = sensor_sample_fetch(pressure_in);
    if (k_timer_status_get(&sensor_check) > 0){

        if (err != 0) {
            //if pressure sensor is not attached
            LOG_ERR("Sensor not attached");
            smf_set_state(SMF_CTX(&s_obj), &device_states[error_S]);
        }
        else {
            //if pressure is attached
            smf_set_state(SMF_CTX(&s_obj), &device_states[measure_S]);
        }

   }
}
static void init_S_exit(void *o)
{
    // starting heartbeat LED timers in main function
    k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_TIME_MS), K_MSEC(HEARTBEAT_TIME_MS));
}

/* State measure_S */
static void measure_S_entry(void *o)
{

    //intialize loop variable and period_counter
    loop_count_1 = 0;
    period_counter_1 = 0; 
    loop_count_2 =0;
    period_counter_2=0;
    val_arr2_full = false;
    val_arr_full = false;
    avg_max2_val = 0;
    avg_min2_val = 0;
    avg_max_val = 0;
    avg_min_val = 0;
    max_sum=0;
    min_sum=0;
    max_sum_2=0;
    min_sum_2=0;
    
    LOG_INF("Current state: measure_S.");
    int ret;
    ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_EDGE_TO_ACTIVE); // enable button1
    ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_EDGE_TO_ACTIVE); // enable button2

    // START SAMPLING TIMERS!!!
    k_timer_start(&adjust_bright1_timer, K_MSEC(BRIGHT1_SAMPLING_MS), K_MSEC(BRIGHT1_SAMPLING_MS));
    k_timer_start(&adjust_bright2_timer, K_USEC(BRIGHT2_SAMPLING_US), K_USEC(BRIGHT2_SAMPLING_US));
    k_timer_start(&vbat_timer, K_SECONDS(VBAT_CHECK_S), K_SECONDS(VBAT_CHECK_S));

}
static void measure_S_run(void *o)
{
    //Reading Pressure Sensor (gets average pressure sensor reading from 10 samples)
    atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);

    //LED1 Brightness Calculation Testing
    if (val_arr_full!=false){

        for (int h=0; h<100; h++){ //for each of 500 periods, scan for 1 max and 1 min 10 times

        for (int h2=(0+(h*10)); h2<(10+(h*10)); h2++){
                
            if (h2==(0+(h*10))){
            //LOG_INF("Entering foor loop?");

                period_min_arr=val_mv_array[h2];
                period_max_arr=val_mv_array[h2];
            }
            else{
                if (val_mv_array[h2]>period_max_arr){
                    period_max_arr = val_mv_array[h2];

                }
                if (val_mv_array[h2]<period_min_arr){
                    period_min_arr = val_mv_array[h2];

                }
                if (h2==(9+(h*10))){
                //LOG_INF("Hello?");
                    max_sum += period_max_arr;
                    min_sum += period_min_arr; 
                    //if (i2==9){
                    //LOG_INF("Max_Sum_2 is at %d", max_sum_2);
                    //}
                    //recorder += 1;
                }
            }

        }


    }

    avg_max_val = max_sum/((float)(100.00));
    avg_min_val = min_sum/((float)(100.00));
    avg_vpp = avg_max_val - avg_min_val; 


    LOG_INF("Avg VPP 1 is %d", avg_vpp);
    bright_led1 = (1.00/45.00)*((float)avg_vpp - (float)5.00);
    if (bright_led1>1){

        bright_led1 = 1;
    }
    if (bright_led1<0){
        bright_led1 = 0; 
    }
    //update LED1 brightness
    int ret;
    ret = pwm_set_pulse_dt(&led1_pwm, (1-(float)bright_led1)*led1_pwm.period);
    if (ret)
    {
        LOG_ERR("Could not set led1_pwm (PWM0)");
    }

    //LOG_INF("Avg VPP 2 is %d", avg_vpp_2);

    max_sum = 0;
    min_sum = 0;
    avg_max_val = 0;
    avg_min_val = 0;
    avg_vpp = 0; 
    memset(val_mv_array, (float)0, sizeof(val_mv_array));
    val_arr_full=false;


    }



    //LED2 Brightness Calculation Testing
    if (val_arr2_full!=false){    

    for (int i=0; i<500; i++){ //for each of 500 periods, scan for 1 max and 1 min 10 times

        for (int i2=(0+(i*10)); i2<(10+(i*10)); i2++){
                
            if (i2==(0+(i*10))){
                period_min_arr2=val_mv_array_2[i2];
                period_max_arr2=val_mv_array_2[i2];
            }
            else{
                if (val_mv_array_2[i2]>period_max_arr2){
                    period_max_arr2 = val_mv_array_2[i2];

                }
                if (val_mv_array_2[i2]<period_min_arr2){
                    period_min_arr2 = val_mv_array_2[i2];

                }
                if (i2==(9+(i*10))){
                
                    max_sum_2 += period_max_arr2;
                    min_sum_2 += period_min_arr2; 
                    recorder += 1;
                }
            }

        }
   
        
    }

    avg_max2_val = max_sum_2/((float)(500.00));
    avg_min2_val = min_sum_2/((float)(500.00));
    avg_vpp_2 = avg_max2_val - avg_min2_val; 


    LOG_INF("Avg Vpp 2 is %d", avg_vpp_2);
    bright_led2 = (1.00/140.00)*((float)avg_vpp_2 - (float)10.00);
    if (bright_led2>1){

        bright_led2=1;

    }
    if (bright_led2<0){

        bright_led2=0;

    }
    //update LED2 brightness
    int ret;
    ret = pwm_set_pulse_dt(&led2_pwm, (1-(float)bright_led2)*led2_pwm.period);
    if (ret)
    {
        LOG_ERR("Could not set led2_pwm (PWM1)");
    }
    max_sum_2 = 0;
    min_sum_2 = 0;
    avg_max2_val = 0;
    avg_min2_val = 0;
    avg_vpp_2 = 0; 
    recorder = 0;
    memset(val_mv_array_2, (float)0, sizeof(val_mv_array_2));
    sanity_check_2=0;
    val_arr2_full=false;


    }
    
    //Checking VBUS 
    int usbregstatus;
    usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) {
        // VBUS detected
        smf_set_state(SMF_CTX(&s_obj), &device_states[vbus_S]);


    } else {
        // VBUS not detected


    }
    
}
static void measure_S_exit(void *o)
{
    k_timer_stop(&adjust_bright1_timer);
    k_timer_stop(&adjust_bright2_timer);
    k_timer_stop(&vbat_timer);
    
    int ret;
    ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE); // disable button1
    ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE); // disable button2

    //turn off LEDs 1 and 2
    ret = pwm_set_pulse_dt(&led1_pwm, led1_pwm.period);
    ret = pwm_set_pulse_dt(&led2_pwm, led2_pwm.period);

}

/* State vbus_S */
static void vbus_S_entry(void *o)
{

    LOG_DBG("Current state: vbus_S.");
    //Blink LED3 at 1Hz (on for 0.5 seconds, off for 0.5 seconds)
    k_timer_start(&led3_blink_timer, K_MSEC(LED3_BLINK_TIME_MS), K_MSEC(LED3_BLINK_TIME_MS));

}
static void vbus_S_run(void *o)
{
    int usbregstatus;
    usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus) {
        // VBUS detected
    } else {
        // VBUS not detected
        smf_set_state(SMF_CTX(&s_obj), &device_states[measure_S]);
    }

}
static void vbus_S_exit(void *o)
{

    k_timer_stop(&led3_blink_timer);

}

/* State error_S */
static void error_S_entry(void *o)
{

    LOG_DBG("Current state: error_S.");
    gpio_pin_toggle_dt(&led3);

}
static void error_S_run(void *o)
{
}
static void error_S_exit(void *o)
{

}

/* Populate state table */
static const struct smf_state device_states[] = {
    [init_S] = SMF_CREATE_STATE(init_S_entry, init_S_run, init_S_exit),
    [measure_S] = SMF_CREATE_STATE(measure_S_entry, measure_S_run, measure_S_exit),
    [vbus_S] = SMF_CREATE_STATE(vbus_S_entry, vbus_S_run, vbus_S_exit),
    [error_S] = SMF_CREATE_STATE(error_S_entry, error_S_run, error_S_exit),
};

/* -----------------------------------------------------------------------------------------------------------   */

/* SECTION 5: void main(void) */

void main(void)
{
    


    

    
    
    
    /* Set initial state */
    smf_set_initial(SMF_CTX(&s_obj), &device_states[init_S]);

    /* Run the state machine */
    while (1)
    {
        int ret;
        ret = smf_run_state(SMF_CTX(&s_obj));
        if (ret)
        {
            /* handle return code and terminate state machine */
            break;
        }

        k_msleep(1);
    }
}

/* -----------------------------------------------------------------------------------------------------------   */

/*

References:

for git cloning:
    https://stackoverflow.com/questions/71708222/cannot-see-cloned-repositry-in-my-local-drive

for meaning of Vpp:
    https://electronics.stackexchange.com/questions/123907/the-various-terms-for-voltage

for using two pwm channels / two pwm leds:
    https://devzone.nordicsemi.com/f/nordic-q-a/95405/adding-pwm-channels-in-zephyr

for troubleshooting log statement errors:
    https://devzone.nordicsemi.com/f/nordic-q-a/69975/__log_level-undeclared-first-use-in-this-function

for estimate of atmospheric pressure in Durham, NC:
    https://www.usairnet.com/weather/maps/current/north-carolina/barometric-pressure/
    https://www.convertunits.com/from/inHg/to/daPa

for pressure sensor code:
    https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-serial-comm/-/blob/main/application/src/pressure.c?ref_type=heads 
    https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-serial-comm/-/blob/main/application/src/main.c?ref_type=heads#L12
    https://gitlab.oit.duke.edu/mhealthtympanometer/mhealth-tymp-firmware/-/blob/main/mhealth-tymp/boards/nrf52833dk_nrf52833.overlay?ref_type=heads

for arrays:
    https://www.programiz.com/cpp-programming/arrays
    https://stackoverflow.com/questions/19631873/c-how-do-i-check-if-an-array-is-full
    https://stackoverflow.com/questions/24130973/assigning-a-zero-to-all-array-elements-in-c

for for loops:
    https://www.programiz.com/c-programming/c-for-loop

for pressure.c, pressure.h, and pressure sensor code across all submitted files:
    https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-serial-comm/-/tree/main/application?ref_type=heads

for timers:
    https://docs.zephyrproject.org/latest/kernel/services/timing/clocks.html

for merge requests:
    https://docs.gitlab.com/ee/user/project/merge_requests/
*/

/* -----------------------------------------------------------------------------------------------------------   */

